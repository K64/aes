import java.util.Scanner;
import java.util.concurrent.ExecutionException;

/**
 * Created by Agnis on 11/3/2014.
 */
public class Sakums {

    Scanner sc;
    private String plain;
    private String key;
    private String cipher;
    private boolean _tukshs;
    private boolean begin = true;
    AES aes;
    public Sakums()
    {
        sc = new Scanner(System.in);
    }

    public void Shifreet() throws Exception
    {
        System.out.println("Luudzu ievadiet tekstu, ko veelaties shifreet:");
        do
        {
            System.out.print("_");
            plain = sc.nextLine();
            this.Reset();
            if(plain.length()<1)
            {
                _tukshs = true;
                System.out.println("Tekstaa jabut vismaz 1 elementam.");
            }
            else
            {
                System.out.println("Pienemts.");
                System.out.println("Ievadiet atsleegu: ");
                this.Reset();
                key = sc.next();
                System.out.println("Paldies.");
                _tukshs = false;
                this.Noshifreet();
            }
        }
        while(_tukshs);
    }
    public void Noshifreet() throws Exception
    {
        aes = new AES(key);
        aes.SetUpKeys();
        System.out.println("Noshifreetais teksts:"+aes.EncryptECB(Utils.toHex(plain)));
    }
    public void Atshifreeshana() throws Exception
    {
        aes = new AES(key);
        aes.SetUpKeys();
        System.out.println("Sakumteksts:"+Utils.MakeReadable(aes.DecryptECB(cipher)));
    }
    public void Atshifreet() throws Exception
    {
        System.out.println("Luudzu ievadiet tekstu, ko veelaties atshifreet:");
        do
        {
            System.out.print("_");
            this.Reset();
            cipher = sc.next();

            if(cipher.length()<32 | cipher.length() % 2 !=0)
            {
                _tukshs = true;
                System.out.println("Tekstam jabut heksadecimalaja formataa!");
            }
            else
            {
                System.out.println("Pienemts.");
                System.out.println("Ievadiet atsleegu: ");
                this.Reset();
                key = sc.next();
                System.out.println("Paldies.");
                _tukshs = false;
                this.Atshifreeshana();
            }
        }
        while(_tukshs);
    }
    public void Sakt() throws Exception
    {
        System.out.println("Teksta shifreeshanas programma, izmantojot AES.");
        System.out.println("*");
        System.out.println("* Veelaaties shifreet(S) vai atshifreet?(A)");
        while(begin)
        {

            char s = sc.next().charAt(0);
            if(s == 'a' | s == 'A')
            {
                this.Reset();
                this.Atshifreet();
                begin = false;
            }
            else if(s== 's' | s== 'S')
            {
                this.Reset();
                this.Shifreet();
                begin = false;
            }
            else
            {
                this.Reset();
                System.out.println("*Nepareiza izveele. Ievadiet '1' vai '2'.");
                begin = true;
            }

        }
    }

    private void Reset()
    {
        sc = new Scanner(System.in);
    }
}
