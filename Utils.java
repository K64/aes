import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.util.ArrayList;

/**
 * Created by Agnis on 1/11/2014, 08:23.
 */
public class Utils {

    public static byte[][][] To3DArray(String text, boolean isPadded) throws UnsupportedEncodingException
    {
        byte[] arr = Utils.HexStringToArray(text);

        int arr_max = arr.length;
        int blocks =1;
        if((arr_max < 16))
            blocks = 1;
        else if((arr_max >=16) & (arr_max % 16==0) & isPadded)
            blocks = arr_max /16;
        else if((arr_max >=16) & (arr_max % 16==0) & !isPadded)
            blocks = arr_max /16 + 1;
        else
            blocks = arr_max /16 + 1;



        int col = blocks * 4;
        int arr_current = 0;

        byte[][][] out = new byte[blocks][4][4];

        for(int b =0; b<blocks;b++)
        {

            for(int c = 0; c<out[0].length;c++)
            {
                byte tmp[];
                tmp = Utils.GetColumn(out[b],c);
                for(int t_p=0;t_p<4;t_p++)
                {
                    if(arr_current < arr_max)
                        tmp[t_p] = arr[arr_current];
                    else if((arr_current == arr_max) & !isPadded)
                        tmp[t_p] = 1;
                    else if (!isPadded)
                        tmp[t_p] = 0;

                    arr_current = arr_current+1;
                }

                out[b] = Utils.SetColumn(out[b],tmp,c);
            }
        }


        return out;

    }
    public static String FlattenToString(byte[][][] src) throws Exception
    {
        ArrayList<Byte> out = new ArrayList<Byte>(50);
        int count =0;
        for(int i=0;i<src.length;i++)
        {
            for(int j =0;j< src[i].length;j++)
            {
                for(int k=0;k< src[i][j].length;k++)
                {
                    out.add(src[i][k][j]);
                    count++;
                }
            }
        }

        byte[] b_dec = new byte[count];
        int count_b =0;
        for(Byte b:out)
        {
            b_dec[count_b] = b.byteValue();
            count_b++;
        }

        return Utils.ArrayToHexString(b_dec);
    }

    public static byte[][] SetColumn(byte[][] arr, byte[] col, int index)
    {
        for(int i =0;i<arr.length;i++)
        {
            arr[i][index] = col[i];
        }
        return arr;
    }
    public static byte[] GetColumn(byte[][] arr, int index)
    {
        byte[] out = new byte[arr.length];

        for(int i =0;i<arr.length;i++)
        {
            out[i] = arr[i][index];
        }
        return out;
    }

    public static void p_out(byte[] arr)
    {
        for(byte b:arr)
            System.out.printf("%02X ",b);
        System.out.println();
    }

    public static void p_out(byte[][] arr)
    {
        for(int i=0;i<arr.length;i++)
        {
            for(int k=0;k<arr[0].length;k++)
            {
                System.out.printf("%02X ",arr[i][k]);
            }
            System.out.println();
        }
    }

    public static void p_out(byte[][][] arr)
    {
        int f =0;
        for(int c =0;c < arr.length;c++)
        {
            for(int i=0;i<arr[c].length;i++)
            {
                for(int k=0;k<arr[c][i].length;k++)
                {
                    System.out.printf("%02X ",arr[c][i][k]);
                    f = f+1;
                }

                System.out.println();
            }
            System.out.printf("-----");
            System.out.println();
        }
    }
    public static String ArrayToHexString(byte[] a)
    {
        StringBuilder sb = new StringBuilder(a.length * 2);
        for(byte b: a)
            sb.append(String.format("%02x", b & 0xff));
        return sb.toString();
    }
    public static byte[] HexStringToArray(String s)
    {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }
    public static String toHex(String arg) throws Exception
    {
        return String.format("%x", new BigInteger(1, arg.getBytes("UTF-8")));
    }

    public static String MakeReadable(String hexFormatted) throws Exception
    {
        byte[] p = Utils.HexStringToArray(hexFormatted);
        StringBuilder d = new StringBuilder(50);
        for (byte b: p)
            d.append((char)b);

        return new String(p,"UTF-8");
    }
}
