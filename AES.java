import java.security.MessageDigest;


/**
 * Created by Agnis on 11/3/2014.
 */
public class AES {

    private String key;
    private byte[][] key_init = new byte[4][4];
    private byte[][] key_full;
    AES_kodols aes;

    public  AES(String Key)
    {
        key = Key;
        aes = new AES_kodols();
    }

    public void SetUpKeys() throws Exception
    {
        this.Transform();
        key_full = aes.GenerateRoundKeys(key_init);
    }

    private void Transform() throws Exception
    {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(key.getBytes("UTF-8"));
        byte[] converted = md.digest();
        int c =0;
        for(int i=0;i<4;i++)
        {
            for(int j=0;j<4;j++)
            {
                key_init[j][i] = converted[c];
                c=c+1;
            }
        }
    }
    public String  EncryptECB(String text) throws Exception
    {
        byte[][][] plain = Utils.To3DArray(text,false);
        byte[][][] cipher = new byte [plain.length][plain[0].length][plain[0][0].length];

        for(int i =0;i<plain.length;i++)
        {
            cipher[i] = aes.EncryptBlockECB(plain[i],key_full);
        }

        return Utils.FlattenToString(cipher);
    }
    public String DecryptECB( String cipher) throws Exception
    {
        byte[][][] cipher_f = Utils.To3DArray(cipher,true);
        byte[][][] plain = new byte [cipher_f.length][cipher_f[0].length][cipher_f[0][0].length];

        for(int i =0;i<cipher_f.length;i++)
        {
            plain[i] = aes.DecryptBlockECB(cipher_f[i],key_full);
        }

        return Utils.FlattenToString(plain);
    }

}
