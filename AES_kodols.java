/**
 * Created by Agnis on 10/25/2014, 00:14.
 */
public final class AES_kodols {

    private byte[][] p_key;
    private byte[][] round_keys;

    public AES_kodols(){}
    public AES_kodols(byte[][] key)
    {
        p_key = key;
    }

    private byte[] RotWord(byte[] column) throws Exception //TODO: make private after test
    {
        if(column.length !=4)
            throw new Exception("masivam jabut ar 4 elemeniem");
        byte[] newarr = column;
        byte b1 = column[0];
        for(int i=0;i<3;i++)
            newarr[i] = newarr[i+1];

        newarr[3] = b1;
        return newarr;
    }
    private byte[] InvRotWord(byte[] column) throws Exception //TODO: make private after test
    {
        if(column.length !=4)
            throw new Exception("masivam jabut ar 4 elemeniem");
        byte[] newarr = column;
        byte b1 = column[3];
        for(int i=3;i>0;i--)
            newarr[i] = newarr[i-1];

        newarr[0] = b1;
        return newarr;
    }
    private byte[] SubBytes(byte[] origin)
    {
        byte[] orig = origin;
        byte[] newarr = new byte[origin.length];
        for(int i=0;i<orig.length;i++)
        {
           newarr[i] = (byte)Tabulas.sbox[(orig[i]>>4 & 0x0f)][((orig[i])& 0x0f)];
        }

        return newarr;
    }
    private byte[] InvSubBytes(byte[] origin)
    {
        byte[] orig = origin;
        byte[] newarr = new byte[origin.length];
        for(int i=0;i<orig.length;i++)
        {
            newarr[i] = (byte)Tabulas.inverse_sbox[(orig[i]>>4 & 0x0f)][((orig[i])& 0x0f)];
        }

        return newarr;
    }
    private byte[][] SubBytes(byte[][] origin)
    {
        byte[][] tmp = origin ;
        for(int i=0;i<origin.length;i++)
            tmp[i] = this.SubBytes(tmp[i]);

        return tmp;
    }
    private byte[][] InvSubBytes(byte[][] origin)
    {
        byte[][] tmp = origin ;
        for(int i=0;i<origin.length;i++)
            tmp[i] = this.InvSubBytes(tmp[i]);

        return tmp;
    }
    private byte[] Rcon(int roundkey)
    {
        byte[] newarr = new byte[4];
        newarr[0] = (byte)Tabulas.Rcon[roundkey-1];
        for(int i=1;i<4;i++)
            newarr[i] = (byte)0x00;
        return newarr;
    }
    private byte[] xor_arr(byte[] b1, byte[] b2) throws IllegalArgumentException
    {
        if(b1.length != b2.length)
            throw new IllegalArgumentException("masivu lielumiem jabut vienadiem");

        byte[] b3 = new byte[b1.length];
        for(int i=0;i<b1.length;i++)
            b3[i] = (byte)(b1[i] ^ b2[i]);

        return b3;
    }
    public byte[][] GenerateRoundKeys(byte[][] key) throws Exception
    {
        byte[][] arr = new byte[4][44]; //atslega visai bloka modifikacijai

        //pirmas 4 kolonnas ir originala sakuma atslega
        for(int i=0;i<4;i++)
        {
            for(int k=0;k<4;k++)
            {
                arr[i][k]= key[i][k];
            }
        }


        for(int i=4;i<44;i++)
        {
            byte[] tmp = new byte[4];
            tmp[0] = arr[0][i-1];
            tmp[1] = arr[1][i-1];
            tmp[2] = arr[2][i-1];
            tmp[3] = arr[3][i-1];

            if(i%4==0) //katras raunda atslegas pirmajai kolonnai izmantojam vel papildus modifikacijas
            {
                tmp = this.RotWord(tmp);
                tmp = this.SubBytes(tmp);
                tmp = this.xor_arr(tmp,this.Rcon(i/4));
            }

            tmp = this.xor_arr(tmp, Utils.GetColumn(arr, i - 4));
            arr = Utils.SetColumn(arr,tmp,i);
        }

        return arr;
    }
    public void GenerateRoundKeys() throws Exception {
        byte[][] arr = new byte[4][44]; //atslega visai bloka modifikacijai

        //pirmas 4 kolonnas ir originala sakuma atslega
        for (int i = 0; i < 4; i++) {
            for (int k = 0; k < 4; k++) {
                arr[i][k] = p_key[i][k];
            }
        }


        for (int i = 4; i < 44; i++) {
            byte[] tmp = new byte[4];
            tmp[0] = arr[0][i - 1];
            tmp[1] = arr[1][i - 1];
            tmp[2] = arr[2][i - 1];
            tmp[3] = arr[3][i - 1];

            if (i % 4 == 0) //katras raunda atslegas pirmajai kolonnai izmantojam vel papildus modifikacijas
            {
                tmp = this.RotWord(tmp);
                tmp = this.SubBytes(tmp);
                tmp = this.xor_arr(tmp, this.Rcon(i / 4));
            }

            tmp = this.xor_arr(tmp, Utils.GetColumn(arr, i - 4));
            arr = Utils.SetColumn(arr, tmp, i);
        }

        round_keys = arr;
    }
    private byte[] MixArray(byte[] arr, byte[][] mmax)
    {
        byte[] out = new byte[4];

        for(int i=0;i<4;i++) // rezultata kolonna
        {
            int b_sum = 0;
            for(int j=0;j<4;j++)
            {
                b_sum = b_sum ^ this.gmul(mmax[i][j], arr[j]);
            }
            out[i] = (byte)b_sum;
        }

        return out;
    }
    private byte[][] MixColumns(byte[][] arr, byte[][] mmatrix)
    {
        byte[][] out = new byte[4][arr.length];

        for(int i=0;i<arr.length;i++)
        {
            byte[] tmp = this.MixArray(Utils.GetColumn(arr,i),mmatrix);
            Utils.SetColumn(out,tmp,i);
        }

        return out;
    }
    private byte gmul(byte a, byte b)  //NOKOMENTEET
    {
       int p = a & 0xFF;
       int q = b & 0xFF;

        int r = 0;
        for (int i = 0; i < 8; i++) {
            if ((q & 0x01) != 0) {
                r ^= p;
            }
            if ((p & 0x80) != 0) {
                p <<= 1;
                p ^= 0x1b;
            } else {
                p <<= 1;
            }
            q >>= 1;
        }
        return (byte)r;

    }
    private byte[][] ShiftRows(byte[][] origin) throws Exception
    {
        byte[][] out = new byte[4][4];
        for(int i=0;i<4;i++)
            out[0][i] = origin[0][i];

        for(int i =1;i<4;i++)
        {
            byte[] tmp = new byte[4];
            System.arraycopy(origin[i],0,tmp,0,4);
            for(int k=i;k>0;k--)
            {
                tmp = this.RotWord(tmp);
            }

            System.arraycopy(tmp,0,out[i],0,4);

        }

        return out;
    }
    private byte[][] InvShiftRows(byte[][] origin) throws Exception
    {
        byte[][] out = new byte[4][4];
        for(int i=0;i<4;i++)
            out[0][i] = origin[0][i];

        for(int i =1;i<4;i++)
        {
            byte[] tmp = new byte[4];
            System.arraycopy(origin[i],0,tmp,0,4);
            for(int k=i;k>0;k--)
            {
                tmp = this.InvRotWord(tmp);
            }

            System.arraycopy(tmp,0,out[i],0,4);

        }

        return out;
    }
    private byte[][] AddRoundKey(byte[][] origin, byte[][] keychain, int round)
    {
        byte[][] out = new byte[origin.length][origin[0].length];
        for(int i=0;i<origin.length;i++)
        {
            for(int j=0;j<origin[0].length;j++)
            {
                out[i][j] = (byte)(origin[i][j] ^ keychain[i][j+(4*round)]);
            }
        }

        return out;
    }
    public byte[][] EncryptBlockECB(byte[][] pt, byte[][] rk) throws Exception
    {
        byte[][] out = new byte[4][4];
        System.arraycopy(pt,0,out,0,pt.length);
        out = this.AddRoundKey(out,rk,0);

        for(int r =1;r<10;r++)
        {
            out = this.SubBytes(out);
            out = this.ShiftRows(out);
            out = this.MixColumns(out,Tabulas.mix_matrix);
            out = this.AddRoundKey(out,rk, r);
        }

        out = this.SubBytes(out);
        out = this.ShiftRows(out);
        out = this.AddRoundKey(out,rk, 10);

        return out;
    }
    public byte[][] DecryptBlockECB(byte[][] pt, byte[][] rk) throws Exception
    {
        byte[][] out = new byte[4][4];
        System.arraycopy(pt,0,out,0,pt.length);
        out = this.AddRoundKey(out,rk,10);

        for(int r =9;r>0;r--)
        {
            out = this.InvShiftRows(out);
            out = this.InvSubBytes(out);
            out = this.AddRoundKey(out,rk, r);
            out = this.MixColumns(out,Tabulas.inv_mix_matrix);
        }

        out = this.InvShiftRows(out);
        out = this.InvSubBytes(out);
        out = this.AddRoundKey(out,rk, 0);

        return out;
    }



}
